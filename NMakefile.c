#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#define __ALLOC
#define __OUTPUT

#define DISSOCIER_ADRESSE( v ) \
	v = NULL

#define NFREE( v ) \
	if( v != NULL ) \
	{ \
		free( v ); \
		DISSOCIER_ADRESSE( v ); \
	}

typedef unsigned int NU32;
typedef signed int NS32;
typedef NU32 NBOOL;

#define NFALSE (NBOOL)0
#define NTRUE (NBOOL)1

#define NERREUR (NU32)~0

// -------------
// Configuration
// -------------

#define TAILLE_MAXIMALE_PARAMETRE_ENTREE		2048

/**
 * Parle beaucoup
 */
static NBOOL CONFIGURATION_EST_VERBOSE = NFALSE;

/**
 * Parle beaucoup trop
 */
NBOOL CONFIGURATION_EST_VERBOSE_PLUS = NFALSE;

/**
 * Pourquoi on en est arrive a parler autant?
 */
NBOOL CONFIGURATION_EST_VERBOSE_PLUS_PLUS = NFALSE;

/**
 * Fichiers separes par des ; indiquant les fichiers ignores
 */
char CONFIGURATION_FICHIER_IGNORE[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "";

/**
 * Dependances recursives dans les headers
 */
NBOOL CONFIGURATION_DEPENDANCE_RECURSIVE = NFALSE;

/**
 * Afficher caractere type d'inclusion
 */
NBOOL CONFIGURATION_AFFICHER_CARACTERE_TYPE_INCLUSION = NTRUE;

/**
 * Repertoire supplementaire a analyser pour la compilation
 */
char CONFIGURATION_COMPILATION_SUPPLEMENTAIRE[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = ".";

/**
 * Commandes preprocesseur
 */
char CONFIGURATION_PREPROCESSEUR[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "";

/**
 * Librairies a linker
 */
char CONFIGURATION_LIBRAIRIE_LINK[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "";

/**
 * Localisation des librairies supplementaires
 */
char CONFIGURATION_LIBRAIRIE_REPERTOIRE_SUPPLEMENTAIRE[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "";

/**
 * Nom executable en sortie
 */
char CONFIGURATION_NOM_EXECUTABLE[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "";

/**
 * Ajouter les repertoire include deduis de l'analyse
 */
NBOOL CONFIGURATION_AJOUTER_REPERTOIRE_INCLUDE_DEDUIS = NFALSE;

/**
 * Fichier a linker en plus
 */
char CONFIGURATION_LIBRAIRIE_SUPPLEMENTAIRE_LINK[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "";

/**
 * Configuration option GCC
 */
char CONFIGURATION_OPTION_GCC[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "Wall;O3";

/**
 * Configuration dossier temporaire obj
 */
char CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "obj";

/**
 * Compilateur
 */
char CONFIGURATION_COMPILATEUR[ TAILLE_MAXIMALE_PARAMETRE_ENTREE ] = "gcc";

// -----------------------------
// namespace OperationRepertoire
// -----------------------------

/**
 * Creer un repertoire
 *
 * @param rep
 *		Le repertoire a creer
 *
 * @return si l'operation s'est bien passee
 */
NBOOL OperationRepertoire_CreerRepertoire( const char *rep )
{
	return !mkdir( rep,
		0755 );
}

// ----------------------------
// namespace OperationCaractere
// ----------------------------

/**
 * Est un chiffre?
 * 
 * @param c
 * 		Le caractere
 * 
 * @return si c'est un chiffre
 */
NBOOL OperationCaractere_EstUnChiffre( char c )
{
	return ( c >= '0'
		&& c <= '9' );
}

/**
 * Est un separateur
 * 
 * @param c
 * 		Le caractere
 * 
 * @return si c'est un separateur
 */
NBOOL OperationCaractere_EstUnSeparateur( char c )
{
	switch( c )
	{
		case ' ':
		case '\n':
		case '\r':
		case '\t':
			return NTRUE;

		default:
			return NFALSE;
	}
}

// -------------------------
// namespace OperationChaine
// -------------------------

/**
 * Est a la fin du fichier?
 * 
 * @param src
 * 		La chaine source
 * @param curseur
 * 		Le curseur dans la chaine
 * 
 * @return si EOF
 */
NBOOL OperationChaine_EstEOF( const char *src,
	NU32 curseur )
{
	return ( curseur >= strlen( src ) );
}

/**
 * Placer le curseur au caractere
 * 
 * @param src
 * 		La chaine a considerer
 * @param c
 * 		Le caractere où se placer
 * @param positionActuelle
 * 		La position actuelle du curseur
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL OperationChaine_PlacerAuCaractere( const char *src,
	char c,
	NU32 *positionActuelle )
{
	// Caractere lu
	char cL;

	// Chercher le caractere
	do
	{
		// Lire
		cL = src[ *positionActuelle ];

		// Avancer le curseur
		(*positionActuelle)++;
	} while( !OperationChaine_EstEOF( src,
		*positionActuelle )
		&& cL != c );

	// OK
	return ( c == cL );
}

/**
 * Lire une chaine entre deux separateurs
 * 
 * @param src
 * 		La chaine source
 * @param c1
 * 		Le delimitateur gauche
 * @param c2
 * 		Le delimitateur droit
 * @param positionActuelle
 * 		La position dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position a la fin de la lecture
 * 
 * @return la chaine lue
 */
__ALLOC char *OperationChaine_LireEntre( const char *src,
	char c1,
	char c2,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Position initiale
	NU32 positionInitiale = *positionActuelle;

	// Buffer
	char *temp;

	// Caractere lu
	char cL;

	// Curseur pour copie
	NU32 curseur = 0;

	// Caractere d'echappement?
	if( c1 != '\0' )
		// Chercher le premier caractere
		if( !OperationChaine_PlacerAuCaractere( src,
			c1,
			positionActuelle ) )
			return NULL;

	// Lire
	do
	{
		// Si le caractere n'est pas le caractere de fin
		if( src[ *positionActuelle ] != c2 )
		{
			// La memoire n'est pas encore allouee?
			if( out == NULL )
			{
				// Allouer la memoire
				if( !( out = calloc( 1 /* + 1 caractere */ + 1 /* \0 */,
					sizeof( char ) ) ) )
					return NULL;

				// Où copier?
				curseur = 0;
			}
			else
			{
				// Enregistrer adresse
				temp = out;

				// Allouer le nouveau espace memoire
				if( !( out = calloc( strlen( temp ) + 1 /* + 1 caractere */ + 1 /* \0 */,
					sizeof( char ) ) ) )
				{
					NFREE( temp );
					return NULL;
				}

				// Copier anciennes donnees
				memcpy( out,
					temp,
					strlen( temp ) );

				// Où placer le prochain caractere?
				curseur = strlen( temp );

				// Liberer buffer
				NFREE( temp );
			}

			// Copier nouveau caractere
			out[ curseur ] = src[ *positionActuelle ];
		}

		// Lire caractere
		cL = src[ *positionActuelle ];

		// Incrementer curseur
		if( !OperationChaine_EstEOF( src,
			*positionActuelle ) )
			(*positionActuelle)++;
	} while( !OperationChaine_EstEOF( src,
			*positionActuelle )
		&& cL != c2 );

	// Restaurer la position initiale si demande
	if( estConserverPosition )
		*positionActuelle = positionInitiale;

	// OK
	return out;
}

/**
 * Lire entre deux caractere
 * 
 * @param src
 * 		La chaine source
 * @param c
 * 		Le delimitateur gauche/droit
 * @param positionActuelle
 * 		La position actuelle
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 * 
 * @return la chaine lue
 */
__ALLOC char *OperationChaine_LireEntre2( const char *src,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return OperationChaine_LireEntre( src,
		c,
		c,
		positionActuelle,
		estConserverPosition );
}

/**
 * Lire a partir de la position actuelle jusqu'au caractere
 * 
 * @param src
 * 		La chaine source
 * @param c
 * 		Le caractere où s'arreter
 * @param positionActuelle
 * 		La position actuelle dans la chaine
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture
 * 
 * @return la chaine lue
 */
__ALLOC char *OperationChaine_LireJusqua( const char *src,
	char c,
	NU32 *positionActuelle,
	NBOOL estConserverPosition )
{
	return OperationChaine_LireEntre( src,
		'\0',
		c,
		positionActuelle,
		estConserverPosition );
}

// --------------------------
// namespace OperationFichier
// --------------------------

/**
 * Obtenir la taille d'un fichier
 * 
 * @param f
 * 		Le fichier
 * 
 * @return la taille du fichier
 */
long OperationFichier_ObtenirTaille( FILE *f )
{
	// Position initiale
	long positionInitiale;

	// Taille du fichier
	__OUTPUT long taille;

	// Obtenir position initiale
	positionInitiale = ftell( f );

	// Aller a la fin du fichier
	fseek( f,
		0,
		SEEK_END );

	// Obtenir taille
	taille = ftell( f );

	// Restaurer position
	fseek( f,
		positionInitiale,
		SEEK_SET );

	// OK
	return taille;
}

/**
 * Obtenir la taille du fichier
 * 
 * @param lien
 * 		Le lien vers le fichier
 * 
 * @return la taille du fichier
 */
long OperationFichier_ObtenirTaille2( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Taille
	__OUTPUT long sortie;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
		return 0;

	// Lire la taille
	sortie = OperationFichier_ObtenirTaille( fichier );

	// Fermer le fichier
	fclose( fichier );

	// OK
	return sortie;
}

/**
 * Le fichier est EOF?
 * 
 * @param f
 * 		Le fichier
 * @param taille
 * 		La taille du fichier
 * 
 * @return Est EOF?
 */
NBOOL OperationFichier_EstEOF2( FILE *f,
	NU32 taille )
{
	// Verifier
	return (NU32)ftell( f ) >= taille;
}

/**
 * Le fichier est EOF?
 * 
 * @param f
 * 		Le fichier
 * 
 * @return Est EOF?
 */
NBOOL OperationFichier_EstEOF( FILE *f )
{
	// La taille
	long taille;

	// Obtenir la taille
	taille = OperationFichier_ObtenirTaille( f );

	// Verifier
	return OperationFichier_EstEOF2( f,
		taille );
}

/**
 * Le fichier existe?
 * 
 * @param lien
 * 		Le lien vers le fichier
 * 
 * @return si le fichier existe
 */
NBOOL OperationFichier_EstExiste( const char *lien )
{
	// Fichier
	FILE *fichier;

	// Ouvrir le fichier
	if( !( fichier = fopen( lien,
		"r" ) ) )
		return NFALSE;
	// Le fichier existe
	else
	{
		// Fermer le fichier
		fclose( fichier );

		// Quitter
		return NTRUE;
	}
}

/**
 * Obtenir prochain caractere
 * 
 * @param f
 * 		Le fichier
 * @param estConserverSeparateur
 * 		On considere les separateurs comme des caracteres
 * @param estConserverPosition
 * 		Restaurer la position apres la lecture?
 * 
 * @return le caractere lu
 */
char OperationFichier_ObtenirProchainCaractere( FILE *f,
	NBOOL estConserverSeparateur,
	NBOOL estConserverPosition )
{
	// Position initiale
	long positionInitiale;
	
	// Sortie
	__OUTPUT char out;
	
	// Enregistrer position initiale
	positionInitiale = ftell( f );
	
	// Lire
	if( estConserverSeparateur )
		out = fgetc( f );
	else
		while( !OperationFichier_EstEOF( f )
			&& OperationCaractere_EstUnSeparateur( out = fgetc( f ) ) )
			;
		
	// Restaurer position initiale
	if( estConserverPosition )
		// Positionner
		fseek( f,
			positionInitiale,
			SEEK_SET );
			
	// OK
	return out;
}

/**
 * Placer le curseur a la prochaine occurence du caractere
 * 
 * @param f
 * 		Le fichier
 * @param c
 * 		Le caractere auquel se placer
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL OperationFichier_PlacerCaractere( FILE *f,
	char c )
{
	// Caractere lu
	char cL;

	// Chercher le caractere
	do
	{
		// Lire caractere
		cL = (char)fgetc( f );
	} while( !OperationFichier_EstEOF( f )
		&& c != cL );

	// OK
	return c == cL;
}

/**
 * Se placer a la chaine
 * 
 * @param f
 * 		Le fichier
 * @param s
 * 		La chaine a laquelle se placer
 * @param estDoitRetournerAuDebut
 * 		On retourne au debut avant de se placer a cette chaine?
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL OperationFichier_PlacerChaine( FILE *f,
	const char *s,
	NBOOL estDoitRetournerAuDebut )
{
	// Curseur
	NU32 curseur = 0;

	// Longueur chaine
	NU32 longueur;

	// Caractere lu
	char cL;

	// Obtenir la taille de la chaine a rechercher
	longueur = strlen( s );

	// Replacer au debut du fichier
	if( estDoitRetournerAuDebut )
		fseek( f,
			0,
			SEEK_SET );

	// Chercher la chaine
	while( curseur < longueur
		&& !OperationFichier_EstEOF( f ) )
	{
		// Obtenir caractere
		cL = (char)fgetc( f );

		// Caractere trouve
		if( cL == s[ curseur ] )
			curseur++;
		// Impossible de trouver le caractere
		else
			curseur = 0;
	}

	// OK?
	return ( curseur >= longueur );
}

/**
 * Lire chaine entre deux delimiteurs
 * 
 * @param f
 * 		Le fichier
 * @param c1
 * 		Le caractere a gauche
 * @param c2
 * 		Le caractere a droite
 * 
 * @return la chaine lue
 */
__ALLOC char *OperationFichier_LireEntre( FILE *f,
	char c1,
	char c2 )
{
	// Sortie
	__OUTPUT char *out = NULL;

	// Buffer
	char *temp;

	// Curseur pour la copie
	NU32 curseur = 0;

	// Caractere lu
	char cL;

	// Trouver le 1er caractere
	if( c1 != '\0' )
		if( !OperationFichier_PlacerCaractere( f,
			c1 ) )
			return NULL;

	// Lire
	do
	{
		// Lire caractere
		do
		{
			cL = (char)fgetc( f );
		} while( OperationCaractere_EstUnSeparateur( cL ) // forcer la lecture si le premier caractere est le caractere de fin
			&& out == NULL );

		// Si le caractere lu n'est pas le caractere de fin
		if( ( ( c2 == '\0' ) ? !OperationCaractere_EstUnSeparateur( cL ) : ( cL != c2 ) ) )
		{
			// Si la memoire n'est pas encore allouee
			if( out == NULL )
			{
				// Allouer la memoire
				if( !( out = calloc( 1 /* + 1 caractere */ + 1 /* \0 */,
					sizeof( char ) ) ) )
					return NULL;

				// Où copier?
				curseur = 0;
			}
			else
			{
				// Enregistrer l'adresse
				temp = out;

				// Allouer a la nouvelle taille
				if( !( out = calloc( strlen( temp ) + 2,
					sizeof( char ) ) ) )
				{
					// Liberer
					NFREE( temp );

					// Quitter
					return NULL;
				}

				// Copier ancienne donnee
				memcpy( out,
					temp,
					strlen( temp ) );

				// Où copier?
				curseur = strlen( temp );

				// Liberer
				NFREE( temp );
			}

			// Copier le nouveau caractere
			out[ curseur ] = cL;
		}
	} while( !OperationFichier_EstEOF( f )
		&& ( ( c2 == '\0' ) ? !OperationCaractere_EstUnSeparateur( cL ) : ( cL != c2 ) ) );

	// OK
	return out;
}

/**
 * Lire entre deux delimiteurs identiques
 * 
 * @param f
 * 		Le fichier
 * @param c
 * 		Le delimiteur gauche/droit
 *
 * @return la chaine lue
 */
__ALLOC char *OperationFichier_LireEntre2( FILE *f,
	char c )
{
	return OperationFichier_LireEntre( f,
		c,
		c );
}

/**
 * Determiner chemin absolu repertoire depuis chemin absolu fichier
 * 
 * @param cheminAbsoluFichier
 * 		Le chemin absolu du fichier
 * 
 * @return le chemin absolu du repertoire contenant le fichier sous la forme "PATH" sans guillemet
 */
__ALLOC char *OperationFichier_DeterminerCheminAbsoluRepertoireDepuisCheminAbsoluFichier( const char *cheminAbsoluFichier )
{
	// Taille du chemin absolu
	NU32 tailleCheminAbsoluRepertoire;
	
	// Sortie
	__OUTPUT char *out;
	
	// Calculer la taille du chemin
	tailleCheminAbsoluRepertoire = strlen( cheminAbsoluFichier );
	while( tailleCheminAbsoluRepertoire > 0
		&& cheminAbsoluFichier[ tailleCheminAbsoluRepertoire ] != '/' )
		tailleCheminAbsoluRepertoire--;
		
	// Allouer la memoire
	if( !( out = calloc( tailleCheminAbsoluRepertoire + 1,
		sizeof( char ) ) ) )
		return NULL;
	
	// Copier
	memcpy( out,
		cheminAbsoluFichier,
		tailleCheminAbsoluRepertoire );
		
	// OK
	return out;
}

// ------------
// struct Liste
// ------------

typedef struct Liste
{
	// Nombre de fichiers
	NU32 nombre;

	// Liste elements
	char **liste;
	
	// Callback destruction
	void ( *callbackDestruction )( void* );
} Liste;

/**
 * Construire liste
 *
 * @return l'instance de la liste
 */
__ALLOC Liste *Liste_Construire( void ( *callbackDestruction )( void* ) )
{
	// Sortie
	__OUTPUT Liste *out;
	
	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( Liste ) ) ) )
		return NULL;
		
	// Enregistrer
	out->callbackDestruction = callbackDestruction;
	
	// OK
	return out;
}

/**
 * Detruire liste fichiers
 *
 * @param this
 *		Cette instance
 */
void Liste_Detruire( Liste **this )
{
	// Iterateur
	NU32 i = 0;

	// Liberer
	if( (*this)->callbackDestruction != NULL )
		for( ; i < (*this)->nombre; i++ )
			(*this)->callbackDestruction( &(*this)->liste[ i ] );
	NFREE( (*this)->liste );
	NFREE( *this );
}

/**
 * Ajouter element
 *
 * @param this
 *		Cette instance
 * @param element
 * 		L'element a ajouter
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL Liste_AjouterElement( Liste *this,
	void *element )
{
	// Nouvelle adresse
	char **nouvelleAdresse;

	// Allouer le nouvel espace
	if( !( nouvelleAdresse = calloc( this->nombre + 1,
		sizeof( char* ) ) ) )
		return NFALSE;

	// Copier
	memcpy( nouvelleAdresse,
		this->liste,
		sizeof( char* ) * this->nombre );

	// Ajouter element
	nouvelleAdresse[ this->nombre ] = element;

	// Incrementer
	this->nombre++;

	// Liberer ancienne adresse
	NFREE( this->liste );

	// Enregistre
	this->liste = nouvelleAdresse;

	// OK
	return NTRUE;
}

// ----------------
// enum TypeFichier
// ----------------
typedef enum TypeFichier
{
	TYPE_FICHIER_HEADER,
	TYPE_FICHIER_SOURCE,
	TYPE_FICHIER_HEADER_CPP,
	TYPE_FICHIER_SOURCE_CPP,

	TYPE_FICHIER_INCONNU,

	TYPES_FICHIER
} TypeFichier;

static const char ExtensionFichier[ TYPES_FICHIER ][ 8 ] =
{
	".h",
	".c",

	".hpp",
	".cpp",
	
	""
};

static const char TypeFichierTexte[ TYPES_FICHIER ][ 32 ] =
{
	"HEADER",
	"SOURCE",
	
	"HEADER CPP",
	"SOURCE CPP",

	"UNKNOWN"
};

// -------------------
// enum TypeDependance
// -------------------
typedef enum TypeDependance
{
	// ""
	TYPE_DEPENDANCE_UTILISATEUR,
	
	// <>
	TYPE_DEPENDANCE_STANDARD,
	
	TYPES_DEPENDANCE
} TypeDependance;

// -----------------
// struct Dependance
// -----------------
typedef struct Dependance
{
	// Lien dependance (Chemin absolu terminant par /)
	char *lien;
	
	// Nom dependance
	char *nom;
	
	// Nom sans extension
	char *nomSansExtension;
	
	// Chemin complet
	char *cheminComplet;
	
	// Type
	TypeDependance type;
} Dependance;

/**
 * Construire dependance
 * 
 * @param lien
 * 		Le lien de la dependance
 * @param nom
 * 		Le nom du fichier de la dependance
 * @param type
 * 		Le type de dependance
 * 
 * @return l'instance
 */
__ALLOC Dependance *Dependance_Construire( const char *lien,
	const char *nom,
	TypeDependance type )
{
	// Sortie
	__OUTPUT Dependance *out;
	
	// Curseur
	NU32 curseur;
	
	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( Dependance ) ) ) )
		return NULL;
	
	// Allouer data
	if( !( out->lien = calloc( strlen( lien ) + 1,
		sizeof( char ) ) )
		|| !( out->nom = calloc( strlen( nom ) + 1,
			sizeof( char ) ) )
		|| !( out->cheminComplet = calloc( strlen( nom ) + strlen( lien ) + 2,
			sizeof( char ) ) ) )
	{
		NFREE( out->nom );
		NFREE( out->lien );
		NFREE( out );
		return NULL;
	}
	
	// Enregistrer
	memcpy( out->lien,
		lien,
		strlen( lien ) );
	memcpy( out->nom,
		nom,
		strlen( nom ) );
	snprintf( out->cheminComplet,
		strlen( nom ) + strlen( lien ) + 2,
		"%s/%s",
		lien,
		nom );
	out->type = type;
	
	// Chercher la position de l'extension
	curseur = 0;
	if( !OperationChaine_PlacerAuCaractere( out->nom,
		'.',
		&curseur )
		|| curseur <= 0 )
	{
		NFREE( out->cheminComplet );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}
	
	// Allouer la memoire
	if( !( out->nomSansExtension = calloc( curseur,
		sizeof( char ) ) ) )
	{
		NFREE( out->cheminComplet );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}
	
	// Copier
	memcpy( out->nomSansExtension,
		out->nom,
		curseur - 1 );
	
	// OK
	return out;
}

/**
 * Detruire dependance
 * 
 * @param this
 * 		Cette instance
 */
void Dependance_Detruire( Dependance **this )
{
	NFREE( (*this)->nomSansExtension );
	NFREE( (*this)->cheminComplet );
	NFREE( (*this)->nom );
	NFREE( (*this)->lien );
	NFREE( (*this) );
}

// --------------
// struct Fichier
// --------------
typedef struct Fichier
{
	// Type
	TypeFichier type;

	// Nom
	char *nom;
	
	// Nom sans extension
	char *nomSansExtension;

	// Chemin vers le fichier
	char *lien;

	// Lien complet relatif
	char *lienComplet;
	
	// Chemin absolu (CheminAbsolu/Fichier)
	char *cheminAbsolu;
	
	// Liste des dependances
	Liste *dependance;
	
	// Chemin absolu du repertoire dans lequel se trouve le fichier
	char *cheminAbsoluRepertoire;
} Fichier;

/**
 * Construire fichier
 *
 * @param nom
 *		Le nom du fichier
 * @param lien
 *		Le lien vers le fichier
 *
 * @return l'instance du fichier
 */
__ALLOC Fichier *Fichier_Construire( const char *nom,
	const char *lien )
{
	// Sortie
	__OUTPUT Fichier *out;

	// Iterateur
	NU32 i;
	
	// Type de fichier
	TypeFichier type = TYPE_FICHIER_INCONNU;
	
	// Chemin absolu
	char cheminAbsolu[ PATH_MAX + 1 ];
	
	// Taille du chemin absolu
	NU32 tailleCheminAbsoluRepertoire;
	
	// Curseur
	NU32 curseur;
	
	// Determiner le type de fichier
	for( i = 0; i < TYPE_FICHIER_INCONNU; i++ )
		if( strstr( nom,
			ExtensionFichier[ i ] ) != NULL
			&& *( strstr( nom,
				ExtensionFichier[ i ] ) + strlen( ExtensionFichier[ i ] ) ) == '\0' )
			type = i;
			
	// Verifier le type
	if( type >= TYPE_FICHIER_INCONNU )
	{
		if( CONFIGURATION_EST_VERBOSE )
			printf( "[IGNORE] \"%s/%s\"\n",
				lien,
				nom );
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( Fichier ) ) ) )
		return NULL;
		
	// Construire la liste de dependances
	if( !( out->dependance = Liste_Construire( (void ( * )( void* ))Dependance_Detruire ) ) )
	{
		NFREE( out );
		return NULL;
	}

	// Allouer data
	if( !( out->nom = calloc( strlen( nom ) + 1,
		sizeof( char ) ) )
		|| !( out->lien = calloc( strlen( lien ) + 1,
			sizeof( char ) ) )
		|| !( out->lienComplet = calloc( strlen( nom ) + strlen( lien ) + 2,
			sizeof( char ) ) ) )
	{
		Liste_Detruire( &out->dependance );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}

	// Enregistrer
	memcpy( out->nom,
		nom,
		strlen( nom ) );
	memcpy( out->lien,
		lien,
		strlen( lien ) );
	snprintf( out->lienComplet,
		strlen( nom ) + strlen( lien ) + 2,
		"%s/%s",
		lien,
		nom );
	out->type = type;
	
	// Determiner le chemin absolu
	realpath( out->lienComplet,
		cheminAbsolu );
		
	// Allouer la memoire
	if( !( out->cheminAbsolu = calloc( strlen( cheminAbsolu ) + 1,
		sizeof( char ) ) ) )
	{
		Liste_Detruire( &out->dependance );
		NFREE( out->lienComplet );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}
	
	// Enregistrer
	memcpy( out->cheminAbsolu,
		cheminAbsolu,
		strlen( cheminAbsolu ) );
		
	// Determiner le chemin absolu du repertoire hebergeant le fichier
	if( !( out->cheminAbsoluRepertoire = OperationFichier_DeterminerCheminAbsoluRepertoireDepuisCheminAbsoluFichier( out->cheminAbsolu ) ) )
	{
		Liste_Detruire( &out->dependance );
		NFREE( out->cheminAbsolu );
		NFREE( out->lienComplet );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}
	
	// Recuperer position .
	curseur = 0;
	if( !OperationChaine_PlacerAuCaractere( out->nom,
		'.',
		&curseur )
		|| curseur <= 0 )
	{
		Liste_Detruire( &out->dependance );
		NFREE( out->cheminAbsoluRepertoire );
		NFREE( out->cheminAbsolu );
		NFREE( out->lienComplet );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}
	
	// Allouer la memoire
	if( !( out->nomSansExtension = calloc( curseur,
		sizeof( char ) ) ) )
	{
		Liste_Detruire( &out->dependance );
		NFREE( out->cheminAbsoluRepertoire );
		NFREE( out->cheminAbsolu );
		NFREE( out->lienComplet );
		NFREE( out->lien );
		NFREE( out->nom );
		NFREE( out );
		return NULL;
	}
		
	// Creer nom sans extension
	memcpy( out->nomSansExtension,
		out->nom,
		curseur - 1 );
	
	// OK
	return out;
}

/**
 * Detruire fichier
 *
 * @param this
 *		Cette instance
 */
void Fichier_Detruire( Fichier **this )
{
	Liste_Detruire( &(*this)->dependance );
	NFREE( (*this)->nomSansExtension );
	NFREE( (*this)->cheminAbsoluRepertoire );
	NFREE( (*this)->cheminAbsolu );
	NFREE( (*this)->lienComplet );
	NFREE( (*this)->lien );
	NFREE( (*this)->nom );
	NFREE( (*this) );
}

/**
 * Compter nombre dependances standards (<>)
 * 
 * @param this
 * 		Cette instance
 * 
 * @return le nombre de dependances standards
 */
NU32 Fichier_ObtenirNombreDependanceStandard( Fichier *this )
{
	// Sortie
	__OUTPUT NU32 out = 0;
	
	// Iterateur
	NU32 i = 0;
	
	// Compter
	for( ; i < this->dependance->nombre; i++ )
		if( ((Dependance**)this->dependance->liste)[ i ]->type == TYPE_DEPENDANCE_STANDARD )
			out++;
			
	// OK
	return out;
}


/**
 * Compter nombre dependances utilisateur ("")
 * 
 * @param this
 * 		Cette instance
 * 
 * @return le nombre de dependances utilisateurs
 */
NU32 Fichier_ObtenirNombreDependanceUtilisateur( Fichier *this )
{
	// Sortie
	__OUTPUT NU32 out = 0;
	
	// Iterateur
	NU32 i = 0;
	
	// Compter
	for( ; i < this->dependance->nombre; i++ )
		if( ((Dependance**)this->dependance->liste)[ i ]->type == TYPE_DEPENDANCE_UTILISATEUR )
			out++;
			
	// OK
	return out;
}

/**
 * Ajouter dependance au fichier
 * 
 * @param this
 * 		Cette instance
 * @param lien
 * 		Lien de la dependance
 * @param nom
 * 		Nom du fichier de la dependance
 * @param type
 * 		Le type de dependance
 * 
 * @return si l'operation a reussi
 */
NBOOL Fichier_AjouterDependance( Fichier *this,
	const char *lien,
	const char *nom,
	TypeDependance type )
{
	// Dependance
	Dependance *dependance;
	
	// Construire dependance
	if( !( dependance = Dependance_Construire( lien,
		nom,
		type ) ) )
		return NFALSE;
		
	// Ajouter dependance
	return Liste_AjouterElement( this->dependance,
		dependance );
}

/**
 * Obtenir dependance depuis chemin complet
 * 
 * @param this
 * 		Cette instance
 * @param cheminCompletDependance
 * 		Le chemin complet de la dependance
 * 
 * @return la dependance
 */
const Dependance *Fichier_ObtenirDependance( const Fichier *this,
	const char *cheminCompletDependance )
{
	// Iterateur
	NU32 i = 0;
	
	// Chercher
	for( ; i < this->dependance->nombre; i++ )
		// Verifier
		if( !strcmp( ((Dependance**)this->dependance->liste)[ i ]->cheminComplet,
			cheminCompletDependance ) )
			return ((Dependance**)this->dependance->liste)[ i ];
			
	// Introuvable
	return NULL;
}

/**
 * Determiner la presence ou non d'un point d'entree dans le fichier
 * 
 * @param this
 * 		L'instance du fichier
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL Fichier_DeterminerPresencePointEntree( Fichier *this )
{
	// TODO
}

/**
 * Generer dependances
 * 
 * @param this
 * 		Cette instance
 * @param cheminAbsoluFichier
 * 		Le chemin absolu du fichier
 * @param cheminAbsoluRepertoire
 * 		Le chemin absolu du repertoire contenant le fichier
 * @param estPremierAppel
 * 		Il s'agit de l'appel initial?
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL Fichier_GenererDependance( Fichier *this,
	const char *cheminAbsoluFichier,
	const char *cheminAbsoluRepertoire,
	NBOOL estPremierAppel )
{	
	// Fichier
	FILE *fichier;
	
	// Buffer
	char buffer[ 2048 ];
	
	// Iterateur
	NU32 i;
	
	// Chemin complet fichier
	char cheminFichierInclusion[ PATH_MAX + 1 ];
	
	// Sauvegarde chemin absolu inclusion
	char sauvegardeCheminFichierInclusion[ PATH_MAX + 1 ];
	
	// Nom fichier inclusion
	char nomFichierInclusion[ PATH_MAX + 1 ];
	
	// Inclusion
	char *inclusion;
	
	// Type de dependance
	TypeDependance type;
	
	// Dependance presente?
	NBOOL estDependancePresente;
	
	// Chemin absolu repertoire
	char *cheminAbsoluRepertoireInclusion;
	
	// Notifier
	if( CONFIGURATION_EST_VERBOSE_PLUS )
	{
		if( estPremierAppel )
			printf( "Dependance de [%s]...\n",
				cheminAbsoluFichier );
		else
			if( CONFIGURATION_EST_VERBOSE_PLUS_PLUS )
				printf( "> Sous dependance (%s)...\n",
					cheminAbsoluFichier );
	}
			
	// Ouvrir le fichier
	if( !( fichier = fopen( cheminAbsoluFichier,
		"r" ) ) )
	{
		snprintf( buffer,
			2048,
			"%s: ",
			cheminAbsoluFichier );
		perror( buffer );
		return NFALSE;
	}

	// Chercher les dependances
	while( OperationFichier_PlacerChaine( fichier,
		"#include",
		NFALSE ) )
	{
		// Determiner le separateur
		switch( OperationFichier_ObtenirProchainCaractere( fichier,
			NFALSE,
			NTRUE ) )
		{
			case '<':
				type = TYPE_DEPENDANCE_STANDARD;
				break;
			case '"':
				type = TYPE_DEPENDANCE_UTILISATEUR;
				break;
				
			default:
				continue;
		}
		
		// Lire inclusion
		if( !( inclusion = OperationFichier_LireEntre( fichier,
			type == TYPE_DEPENDANCE_STANDARD ?
				'<'
				: '"',
			type == TYPE_DEPENDANCE_STANDARD ?
				'>'
				: '"' ) ) )
			continue;
			
		switch( type )
		{
			case TYPE_DEPENDANCE_STANDARD:
				// Enregistrer
				memset( cheminFichierInclusion,
					0,
					PATH_MAX + 1 );
				stpncpy( nomFichierInclusion,
					inclusion,
					PATH_MAX + 1 );
					
				// Liberer inclusion
				NFREE( inclusion );
				break;
			case TYPE_DEPENDANCE_UTILISATEUR:
				// Inclusion absolue?
				if( inclusion[ 0 ] == '/' ) // TODO compatible UNIX seulement pour l'instant (comme snprintf)
					// Copier
					strncpy( cheminFichierInclusion,
						inclusion,
						PATH_MAX + 1 );
				// Inclusion relative
				else
					// Composer chemin complet
					snprintf( cheminFichierInclusion,
						PATH_MAX + 1,
						"%s/%s",
						cheminAbsoluRepertoire,
						inclusion );
						
				// Enregistrer chemin absolu fichier inclusion
				strncpy( sauvegardeCheminFichierInclusion,
					cheminFichierInclusion,
					PATH_MAX + 1 );
					
				// Liberer inclusion
				NFREE( inclusion );
				
				// Vider nom fichier inclusion
				memset( nomFichierInclusion,
					0,
					PATH_MAX + 1 );

				// Extraire chemin
				for( i = strlen( cheminFichierInclusion ); i > 0; i-- )
					// Eliminer nom fichier
					if( cheminFichierInclusion[ i ] == '/' )
					{					
						memcpy( nomFichierInclusion,
							cheminFichierInclusion + i + 1,
							strlen( cheminFichierInclusion ) - i );
						cheminFichierInclusion[ i + 1 ] = '\0';
						break;
					}
				break;
				
			default:
				// Liberer inclusion
				NFREE( inclusion );
				
				// Ignorer
				continue;
		}
		
		// Dependance ajoutee pour la premiere fois?
		if( Fichier_ObtenirDependance( this,
			sauvegardeCheminFichierInclusion ) == NULL )
		{
			// Notifier
			if( CONFIGURATION_EST_VERBOSE_PLUS )
			{
				if( CONFIGURATION_AFFICHER_CARACTERE_TYPE_INCLUSION )
					printf( "%c%s%s%c\n",
						type == TYPE_DEPENDANCE_STANDARD ?
							'<'
							: '"',
						cheminFichierInclusion,
						nomFichierInclusion,
						type == TYPE_DEPENDANCE_STANDARD ?
							'>'
							: '"' );
				else
					printf( "%s%s\n",
						cheminFichierInclusion,
						nomFichierInclusion );
			}
					
			// Ajouter dependance
			if( !Fichier_AjouterDependance( this,
				cheminFichierInclusion,
				nomFichierInclusion,
				type ) )
				continue;
				
			// Determiner si le fichier contient la fonction main( )
			Fichier_DeterminerPresencePointEntree( this );
				
			// Analyser dependance de la dependance
			if( CONFIGURATION_DEPENDANCE_RECURSIVE )
				switch( type )
				{
					case TYPE_DEPENDANCE_UTILISATEUR:
						// Creer le chemin du repertoire
						if( !( cheminAbsoluRepertoireInclusion = OperationFichier_DeterminerCheminAbsoluRepertoireDepuisCheminAbsoluFichier( sauvegardeCheminFichierInclusion ) ) )
							continue;
							
						// Analyser nouvelle dependance
						Fichier_GenererDependance( this,
							sauvegardeCheminFichierInclusion,
							cheminAbsoluRepertoireInclusion,
							NFALSE );
							
						// Liberer
						NFREE( cheminAbsoluRepertoireInclusion );
						break;
						
					default:
						break;
				}
		}
	}
	
	// Fermer fichier
	fclose( fichier );
	
	// OK
	return NTRUE;
}

// ------------
// namespace ::
// ------------

// Couleur shell
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

/**
 * Est fichier autorise dans le makefile
 * 
 * @param fichier
 * 		Le nom du fichier a considerer
 * 
 * @return si le fichier est autorise
 */
NBOOL EstFichierAutorise( const char *fichier )
{
	// Liste des fichiers interdits
	char *listeFichierInterdit;
	
	// Nom fichier interdit
	char fichierInterdit[ PATH_MAX + 1 ];
	
	// Curseur fichier interdit
	NU32 curseurFichierInterdit;

	// Copier adresse liste
	listeFichierInterdit = CONFIGURATION_FICHIER_IGNORE;
	
	// Verifier
	while( *listeFichierInterdit != '\0' )
	{
		// Remettre a zero
		curseurFichierInterdit = 0;
		memset( fichierInterdit,
			0,
			PATH_MAX + 1 );
		
		// Lire le mot interdit
		while( *listeFichierInterdit != ';'
			&& *listeFichierInterdit != '\0' )
		{
			// Copier
			fichierInterdit[ curseurFichierInterdit++ ] = *listeFichierInterdit;
			
			// Avancer
			listeFichierInterdit++;
		}

		// Verifier
		if( !strcmp( fichierInterdit,
			fichier ) )
			return NFALSE;
		
		// Avancer
		if( *listeFichierInterdit == ';' )
			listeFichierInterdit++;
	}
	
	// OK
	return NTRUE;
}

/**
 * Lister repertoire
 *
 * @param nomRepertoire
 *		Le repertoire a lister
 * @param listeFichier
 *		La liste des fichiers
 *
 * @return si l'operation s'est bien passee
 */
NBOOL ListerRepertoire( const char *nomRepertoire,
	Liste *listeFichier )
{
	// Repertoire
	DIR *repertoire;

	// Entite repertoire
	struct dirent *element;

	// Buffer
	char buffer[ 2048 ];
	
	// Fichier
	Fichier *fichier;
	
	// Notifier
	if( CONFIGURATION_EST_VERBOSE )
		printf( "Entre dans \"%s\"\n",
			nomRepertoire );

	// Ouvrir le repertoire
	if( !( repertoire = opendir( nomRepertoire ) ) )
		return NFALSE;

	// Lire elements
	while( ( element = readdir( repertoire ) ) != NULL )
	{
		// Suivant que ce soit un fichier ou un dossier
		switch( element->d_type )
		{
			case DT_DIR:
				// Ne pas rentrer dans le dossier precedent/courant
				if( !strcmp( element->d_name,
					"." )
					|| !strcmp( element->d_name,
						".." ) )
					break;

				// Creer le lien du nouveau repertoire a explorer
				snprintf( buffer,
					2048,
					"%s/%s",
					nomRepertoire,
					element->d_name );

				// Explorer le repertoire
				ListerRepertoire( buffer,
					listeFichier );
				break;

			default:
				// Verifier que le fichier soit autorise
				if( EstFichierAutorise( element->d_name ) )
				{
					// Creer le fichier
					if( !( fichier = Fichier_Construire( element->d_name,
						nomRepertoire ) ) )
						continue;
						
					// Ajouter fichier
					Liste_AjouterElement( listeFichier,
						fichier );
						
					// Traiter les dependances du fichier
					Fichier_GenererDependance( fichier,
						fichier->cheminAbsolu,
						fichier->cheminAbsoluRepertoire,
						NTRUE );
				}
				else
					if( CONFIGURATION_EST_VERBOSE )
						printf( "[IGNORE] \"%s/%s\"\n",
							nomRepertoire,
							element->d_name );
				break;
		}
	}

	// Fermer le repertoire
	closedir( repertoire );

	// OK
	return NTRUE;
}

/**
 * Repertoire include deja present?
 * 
 * @param listeRepertoireInclude
 * 		Liste des repertoires d'include
 * @param repertoire
 * 		Repertoire d'include
 * 
 * @return si le repertoire est deja present
 */
NBOOL EstRepertoireIncludeDejaPresent( Liste *listeRepertoireInclude,
	const char *repertoire )
{
	// Iterateur
	NU32 i = 0;
	
	// Chercher
	for( ; i < listeRepertoireInclude->nombre; i++ )
		// Verifier
		if( !strcmp( repertoire,
			((char**)listeRepertoireInclude->liste)[ i ] ) )
			return NTRUE;
			
	// Introuvable
	return NFALSE;
}

/**
 * Ajouter un repertoire include
 * 
 * @param listeRepertoireInclude
 * 		La liste des repertoires d'include
 * @param repertoire
 * 		Le repertoire a ajouter
 * 
 * @return si l'operation s'est bien passee
 */
NBOOL AjouterRepertoireInclude( Liste *listeRepertoireInclude,
	const char *repertoire )
{
	// Repertoire include traduit
	char repertoireInclude[ PATH_MAX + 1 ];
	
	// Repertoire final
	char *repertoireFinal;
	
	// Traduire le repertoire
	if( !realpath( repertoire,
		repertoireInclude ) )
		return NFALSE;
		
	// Verifier si l'include est deja present
	if( EstRepertoireIncludeDejaPresent( listeRepertoireInclude,
		repertoireInclude ) )
		return NFALSE;
		
	// Allouer la memoire
	if( !( repertoireFinal = calloc( strlen( repertoireInclude ) + 1,
		sizeof( char ) ) ) )
		return NFALSE;
		
	// Copier
	memcpy( repertoireFinal,
		repertoireInclude,
		strlen( repertoireInclude ) );
							
	// Ajouter
	return Liste_AjouterElement( listeRepertoireInclude,
		repertoireFinal );
}

/**
 * Liberer include
 * 
 * @param this
 * 		Cette instance
 */
void LibererInclude( char **this )
{
	NFREE( *this );
}

/**
 * Obtenir l'equivalence d'un fichier c en h
 *
 * @param source
 * 		Le fichier source
 * @param listeFichier
 * 		La liste de tous les fichiers
 * 
 * @return header
 */
const Fichier *ObtenirEquivalenceSourceDependanceHeader( const Fichier *source,
	const Liste *listeFichier )
{
	// Iterateur
	NU32 i = 0;
	
	// Chercher
	for( ; i < listeFichier->nombre; i++ )
		// Verifier
		if( !strcmp( source->nomSansExtension,
			((Fichier**)listeFichier->liste)[ i ]->nomSansExtension )
			&& ( ((Fichier**)listeFichier->liste)[ i ]->type == TYPE_FICHIER_HEADER
				|| ((Fichier**)listeFichier->liste)[ i ]->type == TYPE_FICHIER_HEADER_CPP ) )
			return ((Fichier**)listeFichier->liste)[ i ];
			
	// Introuvable
	return NULL;
}

/**
 * Creer le makefile
 *
 * @param listeFichier
 *		La liste des fichiers
 * @param makefile
 *		Le fichier de sortie
 * @param estVerbose
 * 		Verbose active?
 *
 * @return si l'operation s'est bien passee
 */
NBOOL CreerMakefile( const Liste *listeFichier,
	FILE *makefile )
{
	// Iterateurs
	NU32 i,
		j;
	
	// Liste repertoire include
	Liste *listeRepertoireInclude;
	
	// Data extraite
	char *data;
	NU32 curseur;
	
	// Fichier
	const Fichier *fichier;
		
	// Construire la liste des repertoire
	if( !( listeRepertoireInclude = Liste_Construire( (void ( * )( void* ))LibererInclude ) ) )
		return NFALSE;
	
	// Lister les repertoire d'include
	for( i = 0; i < listeFichier->nombre; i++ )
	{
		// Header
		if( ((Fichier**)listeFichier->liste)[ i ]->type == TYPE_FICHIER_HEADER
			|| ((Fichier**)listeFichier->liste)[ i ]->type == TYPE_FICHIER_HEADER_CPP )
			AjouterRepertoireInclude( listeRepertoireInclude,
				((Fichier**)listeFichier->liste)[ i ]->cheminAbsoluRepertoire );
					
		// Dependances
		for( j = 0; j < ((Fichier**)listeFichier->liste)[ i ]->dependance->nombre; j++ )
			// Traiter uniquement les includes utilisateurs
			switch( ((Dependance**)((Fichier**)listeFichier->liste)[ i ]->dependance->liste)[ j ]->type )
			{
				case TYPE_DEPENDANCE_UTILISATEUR:
					AjouterRepertoireInclude( listeRepertoireInclude,
						((Dependance**)((Fichier**)listeFichier->liste)[ i ]->dependance->liste)[ j ]->lien );
					break;
					
				default:
				case TYPE_DEPENDANCE_STANDARD:
					break;
			}
	}
					
	// Notifier pour verbose
	if( CONFIGURATION_EST_VERBOSE )
	{
		puts( "\n--------------" );
		puts( KGRN"Source/Header:\n"KNRM );
		for( i = 0; i < listeFichier->nombre; i++ )
			printf( "%s (%s), %d dependance%s (%d std, %d utilisateur)\n",
				((Fichier**)listeFichier->liste)[ i ]->cheminAbsolu,
				TypeFichierTexte[ ((Fichier*)listeFichier->liste[ i ])->type ],
				((Fichier**)listeFichier->liste)[ i ]->dependance->nombre,
				((Fichier**)listeFichier->liste)[ i ]->dependance->nombre > 1 ?
					"s"
					: "",
				Fichier_ObtenirNombreDependanceStandard( ((Fichier**)listeFichier->liste)[ i ] ),
				Fichier_ObtenirNombreDependanceUtilisateur( ((Fichier**)listeFichier->liste)[ i ] ) );
		puts( "\n-------------------" );
		puts( KGRN"Repertoire include:\n"KNRM );
		for( i = 0; i < listeRepertoireInclude->nombre; i++ )
			puts( ((char**)listeRepertoireInclude->liste)[ i ] );
	}

	// Creer le makefile
		// Options GCC
			fprintf( makefile,
				"GCC_FLAG=" );
			curseur = 0;
			while( !OperationChaine_EstEOF( CONFIGURATION_OPTION_GCC,
					curseur )
				&& ( data = OperationChaine_LireJusqua( CONFIGURATION_OPTION_GCC,
					';',
					&curseur,
					NFALSE ) ) != NULL )
			{
				// Ecrire
				fprintf( makefile,
					"-%s ",
					data );
				NFREE( data );
			}
			fprintf( makefile,
				"\n" );
		// Repertoire include supplementaire
			if( CONFIGURATION_AJOUTER_REPERTOIRE_INCLUDE_DEDUIS )
			{
				fprintf( makefile,
					"GCC_I=" );
				for( i = 0; i < listeRepertoireInclude->nombre; i++ )
					fprintf( makefile,
						"-I%s ",
						((char**)listeRepertoireInclude->liste)[ i ] );
				fprintf( makefile,
					"\n" );
			}
		// Librairies supplementaires a linker
			fprintf( makefile,
				"GCC_LINK=" );
			curseur = 0;
			while( !OperationChaine_EstEOF( CONFIGURATION_LIBRAIRIE_SUPPLEMENTAIRE_LINK,
					curseur )
				&& ( data = OperationChaine_LireJusqua( CONFIGURATION_LIBRAIRIE_SUPPLEMENTAIRE_LINK,
					';',
					&curseur,
					NFALSE ) ) != NULL )
			{
				// Ecrire
				fprintf( makefile,
					"%s ",
					data );
				NFREE( data );
			}
			fprintf( makefile,
				"\n" );
		// Preprocesseur
			fprintf( makefile,
				"GCC_D=" );
			curseur = 0;
			while( !OperationChaine_EstEOF( CONFIGURATION_PREPROCESSEUR,
					curseur )
				&& ( data = OperationChaine_LireJusqua( CONFIGURATION_PREPROCESSEUR,
					';',
					&curseur,
					NFALSE ) ) != NULL )
			{
				// Ecrire
				fprintf( makefile,
					"-D%s ",
					data );
				NFREE( data );
			}
			fprintf( makefile,
				"\n" );
		// Repertoires supplementaires recherche librairies
			fprintf( makefile,
				"GCC_L=" );
			curseur = 0;
			while( !OperationChaine_EstEOF( CONFIGURATION_LIBRAIRIE_REPERTOIRE_SUPPLEMENTAIRE,
					curseur )
				&& ( data = OperationChaine_LireJusqua( CONFIGURATION_LIBRAIRIE_REPERTOIRE_SUPPLEMENTAIRE,
					';',
					&curseur,
					NFALSE ) ) != NULL )
			{
				// Ecrire
				fprintf( makefile,
					"-L%s ",
					data );
				NFREE( data );
			}
			fprintf( makefile,
				"\n" );
		// Librairies a linker
			fprintf( makefile,
				"GCC_l=" );
			curseur = 0;
			while( !OperationChaine_EstEOF( CONFIGURATION_LIBRAIRIE_LINK,
					curseur )
				&& ( data = OperationChaine_LireJusqua( CONFIGURATION_LIBRAIRIE_LINK,
					';',
					&curseur,
					NFALSE ) ) != NULL )
			{
				// Ecrire
				fprintf( makefile,
					"-l%s ",
					data );
				NFREE( data );
			}
			fprintf( makefile,
				"\n" );
		// Repertoire de sortie
			fprintf( makefile,
				"GCC_TMP=%s\n",
				CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ );
		// Compilateur
			fprintf( makefile,
				"GCC_COMPILER=%s\n\n",
				CONFIGURATION_COMPILATEUR );
				
	
	// Creer regle de generation finale
	fprintf( makefile,
		"%s : %s",
		CONFIGURATION_NOM_EXECUTABLE,
		( strcmp( CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ,
			"." ) ?
			"$(GCC_TMP) "
			: "" ) );
	for( i = 0; i < listeFichier->nombre; i++ )
		switch( ((Fichier**)listeFichier->liste)[ i ]->type )
		{
			case TYPE_FICHIER_SOURCE:
			case TYPE_FICHIER_SOURCE_CPP:
				fprintf( makefile,
					"$(GCC_TMP)/%s.o ",
					((Fichier**)listeFichier->liste)[ i ]->nomSansExtension );
				break;
				
			default:
				break;
		}
	fprintf( makefile,
		"\n\t$(GCC_COMPILER) $(GCC_L) $(GCC_FLAG) -o $@ " );
	for( i = 0; i < listeFichier->nombre; i++ )
		switch( ((Fichier**)listeFichier->liste)[ i ]->type )
		{
			case TYPE_FICHIER_SOURCE:
			case TYPE_FICHIER_SOURCE_CPP:
				fprintf( makefile,
					"%s/%s.o ",
					CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ,
					((Fichier**)listeFichier->liste)[ i ]->nomSansExtension );
				break;
				
			default:
				break;
		}
	fprintf( makefile,
		"$(GCC_LINK) $(GCC_l) " );
	fprintf( makefile,
		"\n\n" );
		
	// Creer regle creation repertoire
	if( strcmp( CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ,
		"." ) )
		fprintf( makefile,
			"$(GCC_TMP) :\n\tmkdir -p $(GCC_TMP)\n\n" );
		
	// Creer sous regles
	for( i = 0; i < listeFichier->nombre; i++ )
		switch( ((Fichier**)listeFichier->liste)[ i ]->type )
		{
			case TYPE_FICHIER_SOURCE:
			case TYPE_FICHIER_SOURCE_CPP:
				fprintf( makefile,
					"$(GCC_TMP)/%s.o : %s ",
					((Fichier**)listeFichier->liste)[ i ]->nomSansExtension,
					((Fichier**)listeFichier->liste)[ i ]->cheminAbsolu );
				if( ( fichier = ObtenirEquivalenceSourceDependanceHeader( ((Fichier**)listeFichier->liste)[ i ],
					listeFichier ) ) != NULL )
					fprintf( makefile,
						"%s ",
						fichier->cheminAbsolu );
				for( j = 0; j < ((Fichier**)listeFichier->liste)[ i ]->dependance->nombre; j++ )
					if( ((Dependance**)((Fichier**)listeFichier->liste)[ i ]->dependance->liste)[ j ]->type == TYPE_DEPENDANCE_UTILISATEUR )
						fprintf( makefile,
							"%s ",
							((Dependance**)((Fichier**)listeFichier->liste)[ i ]->dependance->liste)[ j ]->cheminComplet );
				fprintf( makefile,
					"\n" );
				fprintf( makefile,
					"\t$(GCC_COMPILER) %s$(GCC_D) $(GCC_FLAG) -c %s -o $(GCC_TMP)/%s.o",
					CONFIGURATION_AJOUTER_REPERTOIRE_INCLUDE_DEDUIS ?
						"$(GCC_I) "
						: "",
					((Fichier**)listeFichier->liste)[ i ]->cheminAbsolu,
					((Fichier**)listeFichier->liste)[ i ]->nomSansExtension );
				fprintf( makefile,
					"\n" );
				break;
				
			default:
				break;
		}
		
	// Creer regle clean
	fprintf( makefile,
		"\nclean : \n\trm -f " );
	for( i = 0; i < listeFichier->nombre; i++ )
		fprintf( makefile,
			"$(GCC_TMP)/%s.o ",
			((Fichier**)listeFichier->liste)[ i ]->nomSansExtension );
	if( strcmp( CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ,
		"." ) )
		fprintf( makefile,
			"&& rmdir $(GCC_TMP)" );
	fprintf( makefile,
		"\n" );
		
	// Creer regle mrproper
	fprintf( makefile,
		"\nmrproper : clean\n\trm %s\n",
		CONFIGURATION_NOM_EXECUTABLE );
	
	// Detruire liste repertoire inclusion
	Liste_Detruire( &listeRepertoireInclude );
	
	// OK
	return NTRUE;
}

/**
 * Configurer
 * 
 * @param argc
 * 		Le nombre d'arguments
 * @param argv
 * 		Le vecteur d'arguments
 * 
 * @return 0 si l'operation s'est bien passee, NERREUR en cas d'erreur, et 1 si l'aide a ete demandee
 */
NU32 Configurer( NU32 argc,
	char *argv[ ] )
{
	// Parametre
	char *parametre;
	
	// Liste des options
	const struct option ListeOption[ ] =
	{
		// Verbose etendue
		{ "vv", NFALSE, NULL, 0 },
		
		// Verbose abusee
		{ "vvv", NFALSE, NULL, 0 },
		
		// Fichier a ignorer
		{ "ignore", NTRUE, NULL, 0 },

		// Repertoire supplementaire des sources a compiler
		{ "compile", NTRUE, NULL, 0 },
		
		// Preprocesseur
		{ "pp", NTRUE, NULL, 0 },
		
		// Librairies a linker
		{ "lib", NTRUE, NULL, 0 },
		
		// Repertoire où trouver les librairies
		{ "ulibloc", NTRUE, NULL, 0 },
		
		// Fichiers a linker en plus
		{ "link", NTRUE, NULL, 0 },
		
		// Options gcc
		{ "option", NTRUE, NULL, 0 },
		
		// Compilateur
		{ "program", NTRUE, NULL, 0 },

		// Fin
		{ NULL, NFALSE, NULL, 0 }
	};
	
	// Option
	NS32 option;
	
	// Index option
	NS32 index;
	
	// Lire options
	while( ( option = getopt_long( argc,
		argv,
		"vRihIt:",
		ListeOption,
		&index ) ) != -1 )
	{
		switch( option )
		{
			case 0:
				switch( index )
				{
					case 1:
						CONFIGURATION_EST_VERBOSE_PLUS_PLUS = NTRUE;
					case 0:
						CONFIGURATION_EST_VERBOSE = NTRUE;
						CONFIGURATION_EST_VERBOSE_PLUS = NTRUE;
						break;
						
					default:
						switch( index )
						{
							case 2:
								parametre = CONFIGURATION_FICHIER_IGNORE;
								break;
							case 3:
								parametre = CONFIGURATION_COMPILATION_SUPPLEMENTAIRE;
								break;
							case 4:
								parametre = CONFIGURATION_PREPROCESSEUR;
								break;
							case 5:
								parametre = CONFIGURATION_LIBRAIRIE_LINK;
								break;
							case 6:
								parametre = CONFIGURATION_LIBRAIRIE_REPERTOIRE_SUPPLEMENTAIRE;
								break;
							case 7:
								parametre = CONFIGURATION_LIBRAIRIE_SUPPLEMENTAIRE_LINK;
								break;
							case 8:
								parametre = CONFIGURATION_OPTION_GCC;
								break;
							case 9:
								parametre = CONFIGURATION_COMPILATEUR;
								break;
								
							default:
								return NERREUR;
						}
						
						// Verifier
						if( !optarg
							|| strlen( optarg ) >= TAILLE_MAXIMALE_PARAMETRE_ENTREE )
							return NERREUR;
						
						// Vider
						memset( parametre,
							0,
							TAILLE_MAXIMALE_PARAMETRE_ENTREE );
							
						// Copier
						snprintf( parametre,
							TAILLE_MAXIMALE_PARAMETRE_ENTREE,
							optarg );
						break;
				}
				break;
				
			case 'v':
				CONFIGURATION_EST_VERBOSE = NTRUE;
				break;
			case 'R':
				CONFIGURATION_DEPENDANCE_RECURSIVE = NTRUE;
				break;
			case 'i':
				CONFIGURATION_AFFICHER_CARACTERE_TYPE_INCLUSION = NFALSE;
				break;
			case 'I':
				CONFIGURATION_AJOUTER_REPERTOIRE_INCLUDE_DEDUIS = NTRUE;
				break;
			case 't':
				memset( CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ,
					0,
					TAILLE_MAXIMALE_PARAMETRE_ENTREE );
				memcpy( CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ,
					optarg,
					strlen( optarg ) );
				break;
				
			case 'h':
			default:
				return 1;
		}
	}
	
	// Verifier qu'on ait un executable
	if( optind >= argc )
		return NERREUR;

	// Lire le nom de l'executable
	memcpy( CONFIGURATION_NOM_EXECUTABLE,
		argv[ optind ],
		strlen( argv[ optind ] ) );
	
	// OK
	return 0;
}

/**
 * Afficher l'aide
 */
void AfficherAide( void )
{
	puts( "Generateur de makefile avec gestion des dependances\n" );
	puts( "NMakefile [-vRiIlth] [--vv --vvv --ignore ... --compile ... --pp ... --link ... --ulibloc ... --option ...] NomExecutable\n" );
	puts( "v:\t\tVerbose" );
	puts( "vv:\t\tVerbose+" );
	puts( "vvv:\t\tVerbose++" );
	puts( "R:\t\tRecherche recursive des dependances" );
	puts( "i:\t\tMasquer le caractere de type d'inclusion dans la verbose" );
	puts( "I:\t\tAjouter a la ligne de compilation les includes deduis lors de l'analyse (en cas d'include introuvable)" );
	puts( "l:\t\tLinker des librairies specifiques avec le chemin absolu sous la forme \"path/to/lib1;path/to/lib2/..." );
	printf( "t:\t\tChoisir le dossier temporaire (defaut = %s)\n",
		CONFIGURATION_DOSSIER_TEMPORAIRE_OBJ );
	puts( "h:\t\tAfficher l'aide" );
	puts( "ignore:\t\tIgnorer les fichiers sous la forme \"path/to/f1;path/to/f2/...\"" );
	puts( "compile:\tRepertoires supplementaires contenant des sources requises dans le makefile sous la forme \"path/to/f1;path/to/f2;...\"" );
	puts( "pp:\t\tCommande preprocesseur sous la forme \"PP1;PP2;...\" (gcc -D)" );
	puts( "lib:\t\tLibrairies a linker sous la forme \"lib1;lib2;...\" (gcc -l)" );
	puts( "ulibloc:\tRepertoires supplementaires ou chercher les librairies a linker sous la forme \"path/to/lib1;path/to/lib2\" (gcc -L)" );
	puts( "link:\t\tLinker des fichiers specifiques sous la forme \"path/to/f1;path/to/f2\"" );
	puts( "option:\t\tOptions gcc sous la forme \"option1;option2;...\"" );
	puts( "program:\tLe programme a utiliser pour compiler (Par default gcc)" );
	
	puts( "\n"KBLU"http://nproject.ddns.net/NMakefile/"KWHT"\n" );
}

/**
 * Point d'entree
 * 
 * @param argc
 * 		Le nombre d'arguments
 * @param argv
 * 		Le vecteur d'arguments
 * 
 * @return EXIT_SUCCESS si tout s'est bien passe
 */
NS32 main( NS32 argc,
	char *argv[ ] )
{
	// Makefile en sortie
	FILE *makefile;

	// Liste des fichiers
	Liste *listeFichier;
	
	// Path source
	char pathSource[ 2048 ];
	NU32 curseurPathSource;
	
	// Chemins compilation supplementaires
	char *cheminCompilationSupplementaire;
	
	// Configurer
	switch( Configurer( argc,
		argv ) )
	{
		case NERREUR:
			AfficherAide( );
			return EXIT_FAILURE;
		case 1:
			AfficherAide( );
			return EXIT_SUCCESS;

		default:
			break;
	}

	// Ouvrir le makefile
	if( !( makefile = fopen( "makefile",
		"w+" ) ) )
	{
		perror( "Impossible de creer le makefile: " );
		return EXIT_FAILURE;
	}

	// Creer liste fichier
	if( !( listeFichier = Liste_Construire( (void ( * )( void* ))Fichier_Detruire ) ) )
	{
		fclose( makefile );
		return EXIT_FAILURE;
	}

	// Traiter differents path de source
	cheminCompilationSupplementaire = CONFIGURATION_COMPILATION_SUPPLEMENTAIRE;
	while( *cheminCompilationSupplementaire != '\0'
		&& *cheminCompilationSupplementaire != ';' )
	{
		// Remettre a zero
		curseurPathSource = 0;
		memset( pathSource,
			0,
			2048 );
			
		// Extraire lien
		while( *cheminCompilationSupplementaire != '\0'
			&& *cheminCompilationSupplementaire != ';' )
		{
			pathSource[ curseurPathSource++ ] = *cheminCompilationSupplementaire;
			cheminCompilationSupplementaire++;
		}
		
		// Avancer
		if( *cheminCompilationSupplementaire == ';' )
			cheminCompilationSupplementaire++;

		// Lister les fichiers recursivement
		ListerRepertoire( pathSource,
			listeFichier );
	}

	// Creer le makefile
	CreerMakefile( listeFichier,
		makefile );

	// Detruire liste fichiers
	Liste_Detruire( &listeFichier );

	// Fermer le makefile
	fclose( makefile );

	// OK
	return EXIT_SUCCESS;
}

