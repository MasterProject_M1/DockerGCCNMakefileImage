Docker image for compilation
============================

Description
-----------

This project will build the docker image based on debian
containing gcc and NMakefile.

The build is made through gitlab-ci pipeline. It then pushes
the image to docker.io repository.

Docker hub location
-------------------

https://hub.docker.com/r/nlucassoares/gccnmakefile/

Author
------

Soares Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/MasterProject_M1/DockerGCCNMakefileImage.git
