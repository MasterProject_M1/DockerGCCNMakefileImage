# Dependency
FROM debian

# Author
MAINTAINER Soares Lucas <lucas.soares@orange.com>

# Install dependencies
RUN apt-get update
RUN apt-get install git gcc make cmake unzip libssl-dev libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev -y

# Copy NMakefile
COPY NMakefile.c /tmp/NMakefile.c
RUN cd /tmp && gcc -o NMakefile NMakefile.c && mv NMakefile /usr/bin
RUN rm /tmp/NMakefile.c

# Install libssh2
COPY libssh2.zip /tmp/libssh2.zip
RUN cd /tmp && unzip libssh2.zip && cd libssh2-master && cmake . && make && make install && cd .. && rm -rf *

# Install fmod
COPY fmod.zip /tmp/fmod.zip
RUN cd /tmp && unzip fmod.zip && cp lib/* /usr/local/lib/ && cp lib/* /usr/lib/ && mkdir /usr/local/include/FModex && cp inc/* /usr/local/include/FModex/ && rm -rf *

# Home folder
RUN mkdir /home/docker

# Execute client
ENTRYPOINT /bin/bash
WORKDIR /home/docker
